import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MetroComponent } from './metro/metro.component';
import { MetroDetailComponent } from './metro/metro-detail/metro-detail.component';
import { HeaderComponent } from './header/header.component';
import { DistanceComponent } from './distance/distance.component';
import { MetrListComponent } from './metro/metr-list/metr-list.component';
import { AppRoutingModule } from './app-routing.module';
import {PanelMenuModule} from 'primeng/panelmenu';
import { HttpClientModule }    from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule} from '@angular/forms';
import { DragDropModule } from 'primeng/dragdrop';
import { TableModule } from 'primeng/table';
import {CardModule} from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {OrderListModule} from 'primeng/orderlist';
import { NearbyComponent } from './nearby/nearby.component';

import {ButtonModule} from 'primeng/button';
@NgModule({
  declarations: [
    AppComponent,
    MetroComponent,
    MetroDetailComponent,
    HeaderComponent,
    DistanceComponent,
    MetrListComponent,
    NearbyComponent,
 
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    PanelMenuModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    DragDropModule,
    TableModule,
    CardModule,
    DialogModule,
    BrowserAnimationsModule
    ,OrderListModule,ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
