import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MetroComponent } from './metro/metro.component';
import { MetroDetailComponent } from './metro/metro-detail/metro-detail.component';
import { MetrListComponent } from './metro/metr-list/metr-list.component';
import { DistanceComponent } from './distance/distance.component';
import { NearbyComponent } from './nearby/nearby.component';



const routes: Routes = [
  { path: 'distance', component:DistanceComponent },
  { path: 'metrolist', component: MetrListComponent },
  { path: 'detail', component: MetroDetailComponent },
  { path: 'metro', component: MetroComponent },
  { path: 'nearby', component: NearbyComponent },
 
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}