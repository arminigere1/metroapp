

export class EntityResource<T> {
	
    public status: string;
    public message: string;
    public  payload: T;
    


    public EntityResource( status:string,  message:string,  payload:T) {
        this.status = status;
        this.message = message;
        this.payload = payload;
    }
    
}
