export class Fermata{
    public  id:number;
	public  name:string;
	public longitudine:number;
	public latitudine:number;
	public  linee:string;



	constructor( id:number,name:string, longi:number,lati:number,linee:string){
		this.id=id;
		this.name=name;
		this.longitudine=longi;
		this.latitudine=lati;
		this.linee=linee;
		
  }
}