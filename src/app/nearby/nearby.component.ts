import { Component, OnInit } from '@angular/core';
import { NearByService } from './near-by.service';
import { Neigbour } from '../model/neigbour.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Fermata } from '../model/fermata';



@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.component.html',
  styleUrls: ['./nearby.component.css']
})

export class NearbyComponent implements OnInit {

  
  neighboursList: Neigbour[];
  insertedMetro:string; 
   arrayHasItems:boolean=false; 

 
    

fermata:string;

  constructor(private nearByService: NearByService,private router: Router, private activatedRoute: ActivatedRoute) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.fermata = params['myFermata'];
      
    });

   }

  ngOnInit() {
    this.neighboursList = [];
    
this.SearchMetro();

  

  }  
  
  SearchMetro(){

  
    this.nearByService.getMetrosNear(this.fermata).subscribe(
      nearby =>
      {
        this.neighboursList = nearby.payload;
       
      } );

      if(this.neighboursList.length>0){
              this.arrayHasItems=true;
            
      }


  }





    

   
        

   

 
}
