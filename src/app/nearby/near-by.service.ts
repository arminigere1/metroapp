
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';

import { Neigbour } from '../model/neigbour.model';

import { catchError, map, tap } from 'rxjs/operators';
import { EntityResource } from '../model/payload';



@Injectable({
  providedIn: 'root'
})
export class NearByService {

  private metroNearbyUrl: string = 'http://localhost:4567/nearby';


  constructor(private http: HttpClient) { }

  getMetrosNear(inputparameter:string)  {
     var queryParams = "?metro1="+inputparameter;
    let result = this.http.get<EntityResource<Neigbour[]>>(this.metroNearbyUrl+queryParams);
    console.log(result)
    return result;
  }

}