import { TestBed } from '@angular/core/testing';

import { NearByService } from './near-by.service';

describe('NearByService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NearByService = TestBed.get(NearByService);
    expect(service).toBeTruthy();
  });
});
