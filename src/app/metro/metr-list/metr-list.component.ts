import { Component, OnInit } from '@angular/core';
import { Fermata } from 'src/app/model/fermata';
import { FermataService } from '../metro.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-metr-list',
  templateUrl: './metr-list.component.html',
  styleUrls: ['./metr-list.component.css']
})
export class MetrListComponent implements OnInit {
  mylist: Fermata[];
   
    
  constructor(private fermataservice: FermataService, private router: Router) { }

  ngOnInit() {
    this.mylist = [];
    
    this.fermataservice.getAllMetros().subscribe(allstops => this.mylist=allstops.payload);

  }

  onSelect(fermata:Fermata){

    this.router.navigate(['/nearby'], { queryParams: { myFermata:fermata.name} });
   
  }

}
