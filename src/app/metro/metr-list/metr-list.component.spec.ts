import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetrListComponent } from './metr-list.component';

describe('MetrListComponent', () => {
  let component: MetrListComponent;
  let fixture: ComponentFixture<MetrListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetrListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetrListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
