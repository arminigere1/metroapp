import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetroDetailComponent } from './metro-detail.component';

describe('MetroDetailComponent', () => {
  let component: MetroDetailComponent;
  let fixture: ComponentFixture<MetroDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetroDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
