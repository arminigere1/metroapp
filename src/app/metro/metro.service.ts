import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Distance } from '../distance/distance.model';
import { Fermata } from '../model/fermata';
import { Station } from '../model/station';
import { EntityResource } from '../model/payload';


@Injectable({
  providedIn: 'root'
})
export class FermataService {
private url: string = 'http://localhost:4567/metro/distance';
private allmetroUrl: string = 'http://localhost:4567/allmetro';

distance:Distance
  constructor(private  http: HttpClient) { }

  

  getAllMetros(){
    
    let result=  this.http.get<EntityResource<Fermata[]>>(this.allmetroUrl);
    return result;
  }

  
  test(station:Station): Observable<EntityResource<Distance>> {
    
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    var a= station.metroA;
    var b= station.metroB;
    console.log("Station A  :" + a);
    console.log("Station B :" + b);
   
    var queryParams = "?metro1="+a+"&metro2="+b;
    let result=  this.http.get<EntityResource<Distance>>(this.url+queryParams);
    return result;
    
  
}



}