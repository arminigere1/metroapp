import { Component, OnInit } from '@angular/core';
import { FermataService } from '../metro/metro.service';
import { HttpClient } from '@angular/common/http';
import { Fermata } from '../model/fermata';
import { Station } from '../model/station';
import { Distance } from './distance.model';

@Component({
  selector: 'app-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.css']
})
export class DistanceComponent implements OnInit {

  station:Station;
  mylist: Fermata[];
  selectedFermata: Fermata[] = []; // always inizializza  the array
  draggedFermata: Fermata;
  selectedStop: Fermata;
  TwoElements:boolean; // atleast two elements in array
  display: boolean = false;
  showDistance:  boolean=false
distance:number;
  constructor(private fermataservice: FermataService, private http: HttpClient) { }

  ngOnInit() {
    this.station= new Station();
    this.mylist = [];
    this.fermataservice.getAllMetros().subscribe(allstops => this.mylist=allstops.payload);
  }



  dragStart(event, fermata: Fermata) {
    console.log("dragStart "+fermata);
    this.draggedFermata = fermata;
  }

  drop(event) {
    console.log("drop");
if(this.selectedFermata.length<=1){
    if (this.draggedFermata) {
      let draggedFermataIndex = this.findIndex(this.draggedFermata);
      this.selectedFermata = [...this.selectedFermata, this.draggedFermata];
      this.mylist = this.mylist.filter((val, i) => i != draggedFermataIndex);
     
         if(this.selectedFermata.length==1){
          this.station.metroA= this.draggedFermata.name;
         }
         if(this.selectedFermata.length==2){
        this.station.metroB= this.draggedFermata.name;
        }

      this.draggedFermata = null;

      if(this.station.metroA!=null && this.station.metroB!=null){
        this.fermataservice.test(this.station).subscribe( Distance =>{
                 this.distance=Distance.payload.distance;
          console.log("Distance between  A and B is " + Distance.payload.distance)
        } );
        this.distanceDialog();
        }
     

    }
  }else{
    this.showDialog();
    
  }

  }

  dragEnd(event) {
    console.log("dragEnd");
    this.draggedFermata = null;
  }

  findIndex(fermata: Fermata) {
    let index = -1;
    for (let i = 0; i < this.mylist.length; i++) {
      if (fermata.name === this.mylist[i].name) {
        index = i;
        break;
      }
    }
    return index;
  }

  showDialog() {
    this.display = true;
}

distanceDialog() {
  this.showDistance = true;
}

  removeItem(fermate:Fermata){

  
    this.selectedStop=fermate
    this.findElementToRemove(this.selectedStop);
    this.mylist.push(this.selectedStop);
    console.log("removeItem :" + this.selectedStop.name);

  }

  findElementToRemove(fermata: Fermata):Fermata {
   var found
    for (let i = 0; i < this.mylist.length; i++) {
      if (fermata.name === this.selectedFermata[i].name) {
        found= this.selectedFermata[i];
        this.selectedFermata.pop();
        break;
      }
    }
    return found;
  }


}
